package Day04;

public class Leapyear {
	
	public static boolean isLeapyear(int year) {
		
		if ((year % 400 == 0) || (year % 100 !=0 && year % 4 == 0 ))
			return true;
		
		return false;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
         System.out.println(isLeapyear(2000));
         System.out.println(isLeapyear(1600));
         System.out.println(isLeapyear(1700));
         System.out.println(isLeapyear(1800));
         
	}

}
