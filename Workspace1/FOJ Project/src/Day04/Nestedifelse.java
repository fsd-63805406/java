package Day04;

public class Nestedifelse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
          int sub1 = 88;
          int sub2 = 89;
          int sub3 = 90;
          double total = sub1 + sub2 + sub3;
          double avg = total / 3;
          
          System.out.println("Sub1: " + sub1);
          System.out.println("Sub2: " + sub2);
          System.out.println("Sub3: " + sub3);
          System.out.println("Tot: " + total);
          System.out.println("Avg: " + avg);
          
          if ( sub1 > 39 && sub2 > 39 && sub3 > 39){
        	  if (avg >= 75)
        		  System.out.println("Result: Passed in Distinction");
        	  else if ( avg >= 60)
        		  System.out.println("Result:Passed in I Division");
        	  else if (avg >= 50)
        		  System.out.println("Result:Passed in II Division");
        	  else
        		  System.out.println("Result:Passed in III Division");
          } else {
        	  System.out.println("Result:Failed!!!");
          }
          
	}

}
