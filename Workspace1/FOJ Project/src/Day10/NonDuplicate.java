package Day10;

import java.util.HashSet;

public class NonDuplicate {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 1, 2, 4, 6};

        HashSet<Integer> set = new HashSet<>();
        for (int num : array) {
            set.add(num);
        }

        System.out.print("Non-duplicate values from the array: ");
        for (int num : set) {
            System.out.print(num + " ");
        }
    }
}