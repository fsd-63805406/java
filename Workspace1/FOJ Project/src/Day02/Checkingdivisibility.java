package Day02;

public class Checkingdivisibility {
    
    public static void checkTheNumberIsDivide(int num){
        if (num % 3 == 0 && num % 5 == 0) {
            System.out.println(num + " is divisible by both 3 and 5");
           
        } else if (num % 3 == 0 && num % 5 != 0) {
            System.out.println(num + " is divisible by 3 but not 5");
          
        } else if(num % 3 != 0 && num % 5 == 0){
        	 System.out.println(num + " is divisible by 5 but not 3");
        }
        else {
            System.out.println(num + " is not divisible by both 3 and 5");
           
        }
    }

    public static void main(String[] args) {
        int number = 15;
        checkTheNumberIsDivide(number);      
    }
}
