package Day02;

public class Demo7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
         int num1 = 10;
         int num2 = 20;
         int temp;
         
         System.out.println("Before Swapping / Interchanging");
         System.out.println("num1=" + num1 + "\nnum2 = " + num2 + "\n");
         
         temp = num1;
         num1 = num2;
         num2 = temp;
         
         System.out.println("After Swapping / Interchanging");
         System.out.println("num1=" + num1 + "\nnum2 = " + num2 + "\n");
         
         
         

	}

}
