package Day02;

public class Fizzbizz {
	public static String isDivisibleBy30r5(int num){
		String result = "";
		
		if (num % 3 == 0)
			result += "Fizz";
		if(num % 5 == 0)
			result += "Bizz";
		
		return result;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isDivisibleBy30r5(3));
		System.out.println(isDivisibleBy30r5(5));
		System.out.println(isDivisibleBy30r5(15));

	}

}
