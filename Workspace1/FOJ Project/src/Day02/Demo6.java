package Day02;

public class Demo6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = "25";
		byte b = Byte.parseByte(s); //String to byte
		short sh = Short.parseShort(s); //String to short
		int i = Integer.parseInt(s);  //String to int
		long l = Long.parseLong(s);   //string to long
		
		System.out.println("String s : " + s);
		System.out.println("Byte b : " + b);
		System.out.println("Short sh : " + sh);
		System.out.println("int i : " + i);
		System.out.println("long l : " + l);
		System.out.println();

		l = 45;
		i = (int)l; //type casting to int
		sh = (short) i; //type casting to short
		b = (byte) sh;  //type casting to byte
		s = b + "";     //converting to byte
		
		//higher to lower
		System.out.println("Long l : " + l);
		System.out.println("int i : " + i);
		System.out.println("Short sh : " + sh);
		System.out.println("Byte b: " + b);
		System.out.println("String s : " + s);
		System.out.println();
		
		
		}

}
