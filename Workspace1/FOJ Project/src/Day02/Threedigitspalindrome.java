package Day02;

public class Threedigitspalindrome {
	
	
	public static boolean threeDigitsPalindrome(int num){
		
		return (num / 100) == ( num % 100);
	}

	public static void main(String[] args) {
		System.out.println(isPalindrome(121));
		System.out.println(isPalindrome(124));
	}
}