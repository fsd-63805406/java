package Day02;

public class Threedigitsum {
	
	public static int sumofthreeDigit(int num){
		int unitDigit = num % 10 ;
		int tensDigit = (num /10) % 10 ;
		int hundredDigit = num /100 ;
		return unitDigit + tensDigit + hundredDigit ;
	}

	public static void main(String[] args) {
		int num = 569;
		int sum = sumofthreeDigit(num);
		System.out.println("The sum of three digits number is :- " + sum);
	}

}