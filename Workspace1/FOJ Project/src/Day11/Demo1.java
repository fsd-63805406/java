package Day11;

public class Demo1 {
	
	public static int getVowelsCount(String str){
		int vowelcount = 0;
	       
        for (int i = 0; i < str.length(); i++) {
            char ch = Character.toLowerCase(str.charAt(i)); 
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
                vowelcount++;
            }
        }
        return vowelcount;
	}
	

	public static void main(String[] args) {
		System.out.println(getVowelsCount("aeiou"));
		System.out.println(getVowelsCount("Welcome"));
		System.out.println(getVowelsCount("TalentSprint"));
		System.out.println(getVowelsCount("sky"));
		
		
		

	}

}
